// Imports
const express = require('express')
const app = express()
const port = 3000

// static files
app.use(express.static('public'))
app.use('/css', express.static(__dirname + 'public/css'))
app.use('/img', express.static(__dirname + 'public/img'))
app.use('/index', express.static(__dirname + 'public/index'))

// Listen on port 3000
app.listen(port, () => console.info(`Listening on port ${port}`))